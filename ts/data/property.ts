import * as plugins from '../ul-interfaces.plugins.js';

export class IUplinkProperty {
  wgOrgIdRef: string;
  wgPropertyIdRef: string;
  name: string;
  type: 'website' | 'app' | 'api' | 'other';
  access: 'private' | 'public' | 'auth';
  checkCollectionIdRefs: string[];
}
