import * as search from '../search.js';
import { TCheckResultStatus, TExecutionTiming } from './index.js';

export interface IPageRankCheck {
  inputData: {
    subId: string;
    domain: string;
    searchTerm: string;
    checkBing?: boolean;
    checkGoogle?: boolean;
    bingMinRank?: number;
    googleMinRank?: number;
  };
  executionResults: Array<{
    timing: TExecutionTiming;
    status: TCheckResultStatus;
    pageRankResult: search.IPageRankResult;
  }>;
}
