import { TCheckResultStatus } from './index.js';

export interface IPwaCheck {
  inputData: { domain: string };
  executionResults: Array<{
    subId: string;
    timeStarted: number;
    timeEnded: number;
    duration: number;
    status: TCheckResultStatus;
    data: {
      lhr: any;
      reports: any[];
    };
  }>;
}
