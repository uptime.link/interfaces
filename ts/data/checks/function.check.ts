import { TCheckResultStatus, TExecutionTiming } from './index.js';

export interface IFunctionCheck {
  checkId: string;
  inputData: {
    domain: string;
    functionDef: string;
  };
  executionResults: Array<{
    timing: TExecutionTiming;
    status: TCheckResultStatus;
    data: any;
    checkLog: string[];
  }>;
}
