export interface ISearchResult {
  searchTerm: string;
  targetUrl: string;
  title: string;
  description: string;
  rank: number;
}

/**
 * special data returned by the PageRankCheck check class
 */
export interface IPageRankResult {
  googleRank: number;
  googleBlocked: boolean;
  bingRank: number;
  bingBlocked: boolean;
  searchResults: {
    google: ISearchResult[];
    bing: ISearchResult[];
  };
}
