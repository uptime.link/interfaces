import * as plugins from '../ul-interfaces.plugins.js';
import { ILinkSnapshot } from './linksnapshot.js';

export interface IDomainSnapshot {
  registration: {
    isRegistered: boolean;
    updatedDate: number;
    createdDate: number;
    expiryDate: number;
  };
  delegation: plugins.tsclass.network.IDomainDelegation;
  phishingFlags: {
    listName: string;
  }[];
  recordScans: {
    identifier: string;
    nameservers: string[];
    aRecords: plugins.tsclass.network.IDnsRecord[];
    aaaaRecords: plugins.tsclass.network.IDnsRecord[];
    txtRecords: plugins.tsclass.network.IDnsRecord[];
    mxRecords: plugins.tsclass.network.IDnsRecord[];
    specialRecords: {
      dmarc: plugins.tsclass.network.IDnsRecord[];
    }
  }[];
  linkSnapshots: ILinkSnapshot[];
  whoisServers: {
    serverUrl: string;
    content: string;
  }[];
}
