import * as plugins from '../ul-interfaces.plugins.js';

export interface ILinkSnapshot {
  /**
   * the link base on which the snapshot was taken
   */
  linkBaseUrl: string;
  httpsSupport: boolean;
  httpHttpsParity: boolean;
  httpToHttpsRedirect: boolean;
  fromWwwRedirect: boolean;
  toWwwRedirect: boolean;
  statusCode: number;
  fullPageLoadSize: number;
  fullPageLoadTimeMs: number;
  cookies: any[];
  httpRequest: {
    statusCode: number;
    headers: {
      [key: string]: string;
    }
  };
  httpsRequest: {
    statusCode: number;
    headers: {
      [key: string]: string;
    }
  };
}