/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@uptime.link/interfaces',
  version: '2.0.21',
  description: 'TypeScript interface for the uptime.link API and modules'
}
