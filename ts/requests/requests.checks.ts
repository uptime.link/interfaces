import { ICheckCollection } from '../data/checkcollection.js';
import * as plugins from '../ul-interfaces.plugins.js';

export interface IRequest_CheckExchange
  extends plugins.typedRequestInterfaces.implementsTR<
    plugins.typedRequestInterfaces.ITypedRequest,
    IRequest_CheckExchange
  > {
  method: 'check';
  request: {
    checkCollection: ICheckCollection;
  };
  response: {
    checkCollection: ICheckCollection;
  };
}
