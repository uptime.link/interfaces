import * as plugins from '../ul-interfaces.plugins.js';
import * as data from '../data/index.js';
import { IStatus } from '../data/status.js';

export interface IRequest_Status_Get
  extends plugins.typedRequestInterfaces.implementsTR<
    plugins.typedRequestInterfaces.ITypedRequest,
    IRequest_Status_Get
  > {
  method: 'getStatus';
  request: {
    userToken?: string;
  };
  response: {
    status: IStatus;
  };
}
