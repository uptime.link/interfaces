import * as plugins from '../ul-interfaces.plugins.js';
import * as data from '../data/index.js';

export interface IRequest_Incidents_Get
  extends plugins.typedRequestInterfaces.implementsTR<
    plugins.typedRequestInterfaces.ITypedRequest,
    IRequest_Incidents_Get
  > {
  method: 'getIncidents';
  request: {
    userToken?: string;
  };
  response: {
    currentIncidents: data.IIncident[];
    pastIncidents: data.IIncident[];
  };
}
